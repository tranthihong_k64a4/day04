<!DOCTYPE html>
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Register</title>
<body>
    <?php
        $name = $gender = $gender = $Date = $address= "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if(empty(inputHandling($_POST["fullname"]))){
                echo "<div style='color: red;'>Vui lòng nhập họ và tên</div>";
            }
            if(empty($_POST["gender"])){
                echo "<div style='color: red;'>Vui lòng nhập giới tính</div>";
            }
            if(empty(inputHandling($_POST["depart"]))){
                echo "<div style='color: red;'>Vui lòng nhập phân khoa</div>";
            }
            if(empty(inputHandling($_POST["Date"]))){
                echo "<div style='color: red;'>Vui lòng nhập ngày sinh</div>";
            }
            elseif (!validateDate($_POST["Date"])) {
                echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
            }
        }

        function inputHandling($data) {
            $data = trim($data);
            $data = stripslashes($data);
            return $data;
        }

        function validateDate($date){
            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
                return true;
            } else {
                return false;
            }

        }
    ?>

<form action='' method="post">
        <div style='padding-top:30px'>
            <span class = 'form-group required control-label' style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Họ và tên</span>
            <input style='border:2px solid #FEAFA2;
                            padding:5px 0px' type="text" name="fullname"/>
        </div>
        
        <div style='padding-top:30px'>
            <span class = 'form-group required control-label' style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Giới tính</span>
                    <?php
                    $gender = array(0 => "Nam", 1 => "Nữ"); 
                    for ($i = 0; $i < count($gender); $i++){ ?> 
                        <input type="radio" name="gender" value=".$gender[$i]."/>
                        <label for="<?= $i ?>"> <?= $gender[$i] ?></label>
                    <?php }
                    ?>
        </div>
        
        <div style='padding-top:30px'>
            <span class = 'form-group required control-label' style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Phân khoa</span>
            <select name="depart" style='border:2px solid #FEAFA2; padding:5px 0px'>
            	<?php
                $depart = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                foreach ($depart as $key => $value) { ?>
                	<option value="<?=$key?>"><?=$value ?></option>
                <?php }
                ?>
            </select>
        </div>

        <div style='padding-top:30px'>
            <span class = 'form-group required control-label' style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Ngày sinh</span>
            <input type="date" name="Date" data-date="" data-date-format="DD MM YYYY" placeholder="dd-mm-yyyy" value="" 
            style='border:2px solid #FEAFA2; padding:5px 0px'>
        </div>

        <div style='padding-top:30px'>
            <span style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Địa chỉ</span>
            <input style='border:2px solid #FEAFA2;
                            padding:5px 0px;
                            margin:0px 20px'>
        </div>

        
        <div style='margin-top:20px;margin-left:100px'>
            <button style='background-color:#FEA6B6;
                        padding:10px 20px;
                        border-radius:8px;
                        border-color:#FEA6B6'>Đăng ký</button>
        </div>

        
</form>
<style type="text/css" media="all">
.form-group.required.control-label:after{
   color: red;
   content: "*";
   position: absolute;
}
</style>
</body>
</html>
